const fs = require('fs')
class Env{
  
    constructor(path){
        this.env = require('dotenv')
        
        if(fs.existsSync(`${path}/.env.config`)){
            this.env.config({  
                path: `${path}/.env.config`
            })

        }
        this.path = path
        this.env.config({  
            path: this.path + (process.env.NODE_ENV === "dev" ? ".env.dev" : ".env")
        })

        
        var configs = this.configs(process.env.NODE_ENV)
        for(var x in configs){
            this.envs[x] = configs[x]
        }
     

        this.loadEnvs()
        var file_env = this.get('FILE_ENV')
        if(file_env){
            file_env = this.path + file_env
            if(!fs.existsSync(file_env)){
               console.log('Error, not found file env : '+file_env)
               process.exit(0)
            }

            this.env.config({  
                path: file_env
            })

        }
        this.loadEnvs()
    }

    configs(mode){
        const file_config = this.path + (mode === "dev" ? "dev" : "prod") + '.json'
        var configs = {}
        if(fs.existsSync(file_config)){
            console.log(file_config)
            configs = require(file_config)
            
        }
        this.envs.configs = configs
        return configs
    }
    loadEnvs(){
        for(var x in process.env){
            this.envs[x] = process.env[x]
        }
      
    }
    get(key){
        return this.envs[key]?this.envs[key]:false
    }
}

module.exports = Env